import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class LocalfileService {
  constructor(private _http: HttpClient) {}

  getListIconName = (): Observable<any> => {
    return this._http.get<any>(`../../assets/icon-svg/icon-svg.json`).pipe(
      map((response) => response),
      catchError((error) => error)
    );
  };
}
