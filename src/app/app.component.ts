import { Component } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { LocalfileService } from './services/localfile.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'portfolio';
  private _subscription: Subscription;
  private _loadIcon: boolean
  get loadIcon(){
    return this._loadIcon
  }

  constructor(private _localfileService: LocalfileService, private _matIconRegistry: MatIconRegistry, private _domSanitizer: DomSanitizer) {
    this._subscription = new Subscription();
    this._loadIcon = false
    this._loadIconList()
  }
  private _loadIconList = (): void => {
    this._subscription.add(
      this._localfileService.getListIconName().subscribe((response) => {
        response.forEach((name) => {
          this._matIconRegistry.addSvgIcon(name.replace('.svg', ''), this._domSanitizer.bypassSecurityTrustResourceUrl(`../assets/icon-svg/${name}`));
        });
        this._loadIcon = true
      })
    );
  };
}
